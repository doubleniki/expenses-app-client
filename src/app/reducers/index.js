import { combineReducers } from 'redux';

import app from './reducer.app';

const appReducer = combineReducers({
  app,
});

export default appReducer;
